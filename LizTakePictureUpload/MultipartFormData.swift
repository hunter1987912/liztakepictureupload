//
//  MultipartFormData.swift
//  LizTakePictureUpload
//
//  Created by Liz on 2018/3/2.
//  Copyright © 2018年 楊栗子. All rights reserved.
//

import Foundation
import UIKit

class MultipartFormData {
    
    let boundary = "Boundary-\(UUID().uuidString)"
    var request:URLRequest;
    let body = NSMutableData()
    
    init(url:String) {
        request  = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    }
    
    func setParameters(parameters: [String: String]) {
        for (key, value) in parameters {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
    }
    
    func setUploadImage(image:UIImage, mimeType:String, fileName:String, scale:CGFloat) {
        //mage/jpg, ello.jpg
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(fileName)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(UIImageJPEGRepresentation(image, scale)!)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
    }
    
    
    func send(callback: @escaping() -> Void) {
        
        request.httpBody = body as Data
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let httpResponse = response as? HTTPURLResponse
            if httpResponse?.statusCode == 200 && error == nil{
//                let responseDictionary = try JSONSerialization.jsonObject(with: data!)
//                print("success == \(responseDictionary)")

                Log.debug(String(data: data!, encoding: .utf8)!);
                callback()
            }
            else {
                //Log.debug("statusCode should be 200, but is \(httpResponse?.statusCode)")
                //Log.debug("response = \(response)")
                Log.debug(error.debugDescription)
            }
        }
        task.resume()
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

