//
//  ViewController.swift
//  LizTakePictureUpload
//
//  Created by 楊栗子 on 2018/2/23.
//  Copyright © 2018年 楊栗子. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let uploadUrl = "http://phalcon.liztest.mooo.com/iphone/ajaxUploadFile?source=iphone"
    var loadingAlert:UIAlertController!

    @IBOutlet var imageView:UIImageView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        loadingAlert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        loadingAlert.view.addSubview(loadingIndicator)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 開啟相機
    @IBAction func cameraBtnClick() {
        Log.debug("cameraBtnClick");
        
        //檢查是否有相機可使用
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker: UIImagePickerController = UIImagePickerController()
            picker.sourceType = .camera
            //picker.allowsEditing = true // 可對照片作編輯
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        } else {
            Log.debug("沒有相機鏡頭...") // 用alertView.show
        }
    }
    
    // 開啟相簿
    @IBAction func photoBtnClick() {
        Log.debug("photoBtnClick");
        
//        showAlertLoading()
//        return
        
        //檢查是否有向不可使用
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker: UIImagePickerController = UIImagePickerController()
            picker.sourceType = .photoLibrary
            //picker.allowsEditing = true // 可對照片作編輯
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func saveImageToServerUseInputStream(image: UIImage, name:String) {
        let request = NSMutableURLRequest(url: NSURL(string:uploadUrl)! as URL)
        let session = URLSession.shared
        let image = UIImageJPEGRepresentation(image, 0.1)
        
        request.httpMethod="POST"
        request.setValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
        request.setValue(name, forHTTPHeaderField: "X-FileName")
        request.httpBodyStream = InputStream(data: image!)
        
        let task = session.uploadTask(withStreamedRequest: request as URLRequest)
        task.resume()
    }
    
    func saveImageToServerUseMultipartFormData(image: UIImage, name:String) {
        let iMultipartFormData = MultipartFormData(url:uploadUrl);
        iMultipartFormData.setParameters(parameters: ["param1":"param_value_1"])
        iMultipartFormData.setUploadImage(image: image, mimeType: "image/jpg", fileName: "hello.jpg", scale: 0.1)
        iMultipartFormData.send(callback: { () in
            
            Log.debug("hello callback start");

            // 更新UI只能在main thread
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
                
                self.imageView.image = image
                self.imageView.frame.size.height = self.imageView.frame.width / image.size.width * image.size.height
            }
        
            Log.debug("hello callback end");
        })
    }
    
    func showAlertConfirm() {
        let alert = UIAlertController(title: nil, message: "上傳成功", preferredStyle:.alert)
        let alertAction = UIAlertAction(title: "確定", style: .default);
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil); return;
    }

    /// 取得選取後的照片
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        Log.debug("imagePickerController");
        
        // 關閉相簿
        picker.dismiss(animated: true, completion: nil)
        
        //加入loading
        present(loadingAlert, animated: true, completion: nil)
        
        // 從Dictionary取出原始圖檔
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //上傳到Server
        self.saveImageToServerUseMultipartFormData(image: image, name: "file_name")
        
        //存檔案相簿
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil）
    }
    
    // 圖片picker控制器任務結束回呼
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        Log.debug("imagePickerController");
        picker.dismiss(animated: true, completion: nil)
    }
    
}
